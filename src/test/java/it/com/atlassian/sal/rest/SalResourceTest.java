package it.com.atlassian.sal.rest;

import com.atlassian.sal.rest.Application;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import org.junit.Test;

import javax.ws.rs.core.UriBuilder;

import static org.junit.Assert.*;

public final class SalResourceTest
{
    private static final String API_VERSION = System.getProperty("api.version", "latest");

    @Test
    public void testApplication()
    {
        final WebResource sal = sal().path("/");
        final Application application = sal.get(Application.class);

        assertEquals(APPLICATION_NAME, application.name);
        assertEquals(APPLICATION_VERSION, application.version);
        assertTrue(Integer.valueOf(application.buildNumber) > 0);
        assertNotNull(application.buildDate);
    }

    private WebResource sal()
    {
        return Client.create().resource(getUriBuilder().path("rest").path("sal").path(API_VERSION).build());
    }

    public static UriBuilder getUriBuilder()
    {
        return UriBuilder.fromUri("http://localhost/").port(Integer.parseInt(PORT)).path(CONTEXT);
    }

    private static final String PORT = System.getProperty("http.port", "5990");
    private static final String CONTEXT = System.getProperty("context.path", "/refapp");
    private static final String APPLICATION_NAME = System.getProperty("sal.name", "RefImpl");
    private static final String APPLICATION_VERSION = System.getProperty("sal.version", "1.0");
}
