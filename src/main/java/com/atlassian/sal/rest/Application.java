package com.atlassian.sal.rest;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement(name = "application")
public final class Application
{
    @XmlElement
    public String name;

    @XmlElement
    public String version;

    @XmlElement
    public String buildNumber;

    @XmlElement
    public Date buildDate;
}
