package com.atlassian.sal.rest;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.ApplicationProperties;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import static com.google.common.base.Preconditions.checkNotNull;
import static javax.ws.rs.core.MediaType.*;

@Path("/")
@Consumes({APPLICATION_XML, APPLICATION_JSON})
@Produces({APPLICATION_XML, APPLICATION_JSON})
@AnonymousAllowed
public final class SalResource
{
    private final ApplicationProperties applicationProperties;

    public SalResource(ApplicationProperties applicationProperties)
    {
        this.applicationProperties = checkNotNull(applicationProperties);
    }

    @GET
    public Application get()
    {
        final Application app = new Application();
        app.name = applicationProperties.getDisplayName();
        app.version = applicationProperties.getVersion();
        app.buildNumber = applicationProperties.getBuildNumber();
        app.buildDate = applicationProperties.getBuildDate();

        return app;
    }
}
